/*****************************************************************************
 * png.c: png decoder module making use of libpng.
 *****************************************************************************
 * Copyright (C) 1999-2001 the VideoLAN team
 * $Id$
 *
 * Authors: Leon Moctezuma <densedev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_codec.h>

typedef struct QT_atom_header_s
{

    uint32_t i_size;
    uint32_t i_type;
    uint32_t i_AtomID;

    uint16_t i_reserved1;
    uint16_t i_child_count;

    uint32_t i_reserved2;

} QT_atom_header_t;

typedef struct QT_cufa_s
{

    float i_orientation[4];
    float i_center[2];
    float i_aspect;
    float i_skew;

} QT_cufa_t;

typedef struct QT_sample_pano_s {

	uint16_t i_majorVersion;
	uint16_t i_minorVersion;

	uint32_t i_imageRefTrackIndex;
	uint32_t i_hotSpotRefTrackIndex;

	float f_minPan;
	float f_maxPan;
	float f_minTilt;
	float f_maxTilt;
	float f_minFieldOfView;
	float f_maxFieldOfView;
	float f_defaultPan;
	float f_defaultTilt;
	float f_defaultFieldOfView;

	uint32_t i_imageSizeX;
	uint32_t i_imageSizeY;

	uint16_t i_imageNumFramesX;
	uint16_t i_imageNumFramesY;

	uint32_t i_hotSpotSizeX;
	uint32_t i_hotSpotSizeY;

	uint16_t i_hotSpotNumFramesX;
	uint16_t i_hotSpotNumFramesY;

	uint32_t i_flags;
	uint32_t i_panoType;
	uint32_t i_reserved2;

} QT_sample_pano_t;

typedef struct QT_cuvw_s {

	float f_minPan;
	float f_maxPan;
	float f_minTilt;
	float f_maxTilt;
	float f_minFieldOfView;
	float f_maxFieldOfView;
	float f_defaultPan;
	float f_defaultTilt;
	float f_defaultFieldOfView;

} QT_cuvw_t;

typedef struct QT_node_pano_s
{
        QT_sample_pano_t* p_sample;
        QT_cufa_t* p_cufa;
        QT_cuvw_t* p_cuvw;
} QT_node_pano_t;

/*****************************************************************************
 * Local prototypes
 *****************************************************************************/
static int  OpenDecoder   ( vlc_object_t * );
static void CloseDecoder  ( vlc_object_t * );
static picture_t *Decode( decoder_t *p_dec, block_t **pp_block );
void BEndianToLEndian32( uint32_t* p_value );
void BEndianToLEndian16( uint16_t* p_value );

static inline uint16_t qtvr_Get2BytesInt( uint8_t** pp_buffer );
static inline uint32_t qtvr_Get4BytesInt( uint8_t** pp_buffer );
static inline uint32_t qtvr_Get4BytesFloat( uint8_t** pp_buffer );

static QT_cuvw_t* qtvr_read_cuvw( decoder_t *p_dec, uint8_t** pp_buffer );
static QT_cufa_t* qtvr_read_cufa( decoder_t *p_dec, uint8_t** pp_buffer );
static QT_sample_pano_t* qtvr_read_pdat( decoder_t *p_dec, uint8_t** pp_buffer );
static void qtvr_read_QTAtom( decoder_t *p_dec, uint8_t** pp_buffer );

/*****************************************************************************
 * Quicktime movies are in BigEndian format, so when reading on a PC or other 
 * Little-Endian machine, we've got to swap the bytes around.
 *****************************************************************************/

// Converts a 4-bytes int to reverse the Big-Little Endian order of the bytes.
void BEndianToLEndian32(uint32_t *value)
{
    char* n, b1,b2,b3,b4;
    n = (char *)value;

    b1 = n[0];
    b2 = n[1];
    b3 = n[2];
    b4 = n[3];

    n[0] = b4;
    n[1] = b3;
    n[2] = b2;
    n[3] = b1;
}

// Converts a 2-bytes int to reverse the Big-Little Endian order of the bytes.
void BEndianToLEndian16(uint16_t *value)
{
    char* n, b1,b2;
    n = (char *)value;

    b1 = n[0];
    b2 = n[1];

    n[0] = b2;
    n[1] = b1;
}

static inline uint16_t qtvr_Get2BytesInt( uint8_t** pp_buffer )
{
    uint8_t* p_buffer = *pp_buffer;
    uint16_t data16 = ((uint16_t*)p_buffer)[0];
    BEndianToLEndian16(&data16);

    *pp_buffer += 2;

    return data16;
}

static inline uint32_t qtvr_Get4BytesInt( uint8_t** pp_buffer )
{
    uint8_t* p_buffer = *pp_buffer;
    uint32_t data32 = ((uint32_t*)p_buffer)[0];
    BEndianToLEndian32(&data32);

    *pp_buffer += 4;

    return data32;
}

static inline uint32_t qtvr_Get4BytesFloat( uint8_t** pp_buffer )
{
    uint8_t* p_buffer = *pp_buffer;
    float data32 = ((float*)p_buffer)[0];
    BEndianToLEndian32((uint32_t*)&data32);

    *pp_buffer += 4;

    return data32;
}

static QT_cuvw_t* qtvr_read_cuvw( decoder_t *p_dec, uint8_t** pp_buffer )
{
   QT_cuvw_t* p_cuvw;
   uint8_t* p_buffer = *pp_buffer;

   p_cuvw = ( QT_cuvw_t * )calloc(1, sizeof(QT_cuvw_t ) );

   p_cuvw->f_minPan = qtvr_Get4BytesFloat(&p_buffer);
   p_cuvw->f_maxPan = qtvr_Get4BytesFloat(&p_buffer);
   p_cuvw->f_minTilt = qtvr_Get4BytesFloat(&p_buffer);
   p_cuvw->f_maxTilt = qtvr_Get4BytesFloat(&p_buffer);
   p_cuvw->f_minFieldOfView = qtvr_Get4BytesFloat(&p_buffer);
   p_cuvw->f_maxFieldOfView = qtvr_Get4BytesFloat(&p_buffer);
   p_cuvw->f_defaultPan = qtvr_Get4BytesFloat(&p_buffer);
   p_cuvw->f_defaultTilt = qtvr_Get4BytesFloat(&p_buffer);
   p_cuvw->f_defaultFieldOfView = qtvr_Get4BytesFloat(&p_buffer);

   return p_cuvw;
}

static QT_cufa_t* qtvr_read_cufa( decoder_t *p_dec, uint8_t** pp_buffer )
{
   QT_cufa_t* p_cufa;
   uint8_t* p_buffer = *pp_buffer;

   p_cufa = (QT_cufa_t *)calloc(1, sizeof(QT_cufa_t) );

   p_cufa->i_orientation[0] = qtvr_Get4BytesFloat(&p_buffer);
   p_cufa->i_orientation[1] = qtvr_Get4BytesFloat(&p_buffer);
   p_cufa->i_orientation[2] = qtvr_Get4BytesFloat(&p_buffer);
   p_cufa->i_orientation[3] = qtvr_Get4BytesFloat(&p_buffer);

   p_cufa->i_center[0] = qtvr_Get4BytesFloat(&p_buffer);
   p_cufa->i_center[1] = qtvr_Get4BytesFloat(&p_buffer);

   p_cufa->i_aspect = qtvr_Get4BytesFloat(&p_buffer);
   p_cufa->i_skew = qtvr_Get4BytesFloat(&p_buffer);

   return p_cufa;
}

static QT_sample_pano_t* qtvr_read_pdat( decoder_t *p_dec, uint8_t** pp_buffer )
{

   QT_sample_pano_t* p_sample;
   uint8_t* p_buffer = *pp_buffer;

   p_sample = (QT_sample_pano_t *)calloc(1, sizeof(QT_sample_pano_t) );

   p_sample->i_majorVersion = qtvr_Get2BytesInt(&p_buffer);
   p_sample->i_minorVersion = qtvr_Get2BytesInt(&p_buffer);
   p_sample->i_imageRefTrackIndex = qtvr_Get4BytesInt(&p_buffer);
   p_sample->i_hotSpotRefTrackIndex = qtvr_Get4BytesInt(&p_buffer);
   p_sample->f_minPan = qtvr_Get4BytesFloat(&p_buffer);
   p_sample->f_maxPan = qtvr_Get4BytesFloat(&p_buffer);
   p_sample->f_minTilt = qtvr_Get4BytesFloat(&p_buffer);
   p_sample->f_maxTilt = qtvr_Get4BytesFloat(&p_buffer);
   p_sample->f_minFieldOfView = qtvr_Get4BytesFloat(&p_buffer);
   p_sample->f_maxFieldOfView = qtvr_Get4BytesFloat(&p_buffer);
   p_sample->f_defaultPan = qtvr_Get4BytesFloat(&p_buffer);
   p_sample->f_defaultTilt = qtvr_Get4BytesFloat(&p_buffer);
   p_sample->f_defaultFieldOfView = qtvr_Get4BytesFloat(&p_buffer);
   p_sample->i_imageSizeX = qtvr_Get4BytesInt(&p_buffer);
   p_sample->i_imageSizeY = qtvr_Get4BytesInt(&p_buffer);
   p_sample->i_imageNumFramesX = qtvr_Get2BytesInt(&p_buffer);
   p_sample->i_imageNumFramesY = qtvr_Get2BytesInt(&p_buffer);
   p_sample->i_hotSpotNumFramesX = qtvr_Get2BytesInt(&p_buffer);
   p_sample->i_hotSpotNumFramesY = qtvr_Get2BytesInt(&p_buffer);
   p_sample->i_hotSpotSizeX = qtvr_Get4BytesInt(&p_buffer);
   p_sample->i_hotSpotSizeY = qtvr_Get4BytesInt(&p_buffer);
   p_sample->i_flags = qtvr_Get4BytesInt(&p_buffer);

   p_sample->i_panoType = ((uint32_t*)p_buffer)[0];
   p_buffer +=4;
    
   p_sample->i_reserved2 = qtvr_Get4BytesInt(&p_buffer);

   msg_Err( p_dec, "panoType 0x%X (%4.4s)", p_sample->i_panoType, (char*)&p_sample->i_panoType );

   return p_sample;
}

static int qtvr_ReadQTAtom( decoder_t *p_dec, uint8_t** pp_buffer, QT_node_pano_t *p_node )
{
    int i = 0;
    uint8_t* p_buffer = *pp_buffer;
    uint8_t* p_pos = *pp_buffer;

    QT_atom_header_t header;

    header.i_size = qtvr_Get4BytesInt(&p_buffer);
    msg_Dbg( p_dec, "Atom size %d ", header.i_size );

    if( header.i_size <= 0 ) return -1;

    header.i_type = ((uint32_t*)p_buffer)[0];
    msg_Dbg( p_dec, "-> type %4.4s ", (char*)&header.i_type );
    p_buffer +=4;
    
    header.i_AtomID = qtvr_Get4BytesInt(&p_buffer);
    header.i_reserved1 = qtvr_Get2BytesInt(&p_buffer);
    header.i_child_count = qtvr_Get2BytesInt(&p_buffer);
    msg_Dbg( p_dec, "-> childs %d ", header.i_child_count );
    header.i_reserved2 = qtvr_Get4BytesInt(&p_buffer);

    switch( header.i_type )
    {
        case VLC_FOURCC('p','d','a','t'):
            p_node->p_sample = qtvr_read_pdat( p_dec, &p_buffer );
        break;
        case VLC_FOURCC('c','u','f','a'):
            p_node->p_cufa = qtvr_read_cufa( p_dec, &p_buffer );
        break;
        case VLC_FOURCC('c','u','v','w'):
            p_node->p_cuvw = qtvr_read_cuvw( p_dec, &p_buffer );
        break;
        case VLC_FOURCC('s','e','a','n'):
            for(i = 0; i< header.i_child_count; i++)
                qtvr_ReadQTAtom( p_dec, &p_buffer , p_node );
        break;
        default:
            msg_Err( p_dec, "UNKNOWN");
        break;
    }

    *pp_buffer = p_pos+header.i_size;

  return 1;

}


/*****************************************************************************
 * decoder_sys_t : qtvr decoder descriptor
 *****************************************************************************/
struct decoder_sys_t
{
    bool b_error;
};

/*****************************************************************************
 * Module descriptor
 *****************************************************************************/
vlc_module_begin ()
    set_category( CAT_INPUT )
    set_subcategory( SUBCAT_INPUT_VCODEC )
    set_description( N_("Panorama track decoder (QTVR)") )
    set_capability( "decoder", 100 )
    set_callbacks( OpenDecoder, CloseDecoder )
vlc_module_end ()

/*****************************************************************************
 * OpenDecoder: probe the decoder and return score
 *****************************************************************************
 * Tries to launch a decoder and return score so that the interface is able
 * to chose.
 *****************************************************************************/
static int OpenDecoder( vlc_object_t *p_this )
{
    decoder_t     *p_dec = (decoder_t*)p_this;
    decoder_sys_t *p_sys;
    vlc_value_t    val;

    switch( p_dec->fmt_in.i_codec )
    {
        case VLC_FOURCC('p','a','n','o') : 
            break;
        case VLC_FOURCC('q','t','v','r') :
            break;
        default:
            return VLC_EGENERIC;
    }
    p_dec->fmt_out.i_cat = VIDEO_ES;

    msg_Err( p_dec, "QTVR decoder selected!!!");

    p_dec->pf_decode_video = Decode;

    /* Allocate the memory needed to store the decoder's structure */
    p_dec->p_sys = p_sys = calloc( 1, sizeof( decoder_sys_t ) );
    if( p_sys == NULL )
        return VLC_ENOMEM;

    return VLC_SUCCESS;
}

/*****************************************************************************
 * Decode:
 *****************************************************************************/
static picture_t *Decode( decoder_t *p_dec, block_t **pp_block )
{
    decoder_sys_t *p_sys = p_dec->p_sys;
    block_t *p_block;
    //pano_sample_t* pdat = NULL;
    //uint32_t data32;
    //uint16_t data16;
    uint32_t i;
    uint32_t size;
    uint8_t* p_buffer;
    
    QT_node_pano_t* p_node;
    p_node = ( QT_node_pano_t * )malloc( sizeof( QT_node_pano_t ) );

    if( p_dec->fmt_in.i_codec == VLC_FOURCC('q','t','v','r') ) return NULL;

    if( pp_block == NULL || *pp_block == NULL )
    {
        msg_Err( p_dec, "QTVR without block!!!");
        return NULL;
    }
    p_block = *pp_block;

    p_buffer = p_block->p_buffer+12;

    size = p_block->i_buffer;

    qtvr_ReadQTAtom( p_dec , &p_buffer , p_node );
    
    if( p_node->p_sample )
    {
        FREENULL( p_node->p_sample );
    }

    if( p_node->p_cufa )
    {
        FREENULL( p_node->p_sample );
    }

    if( p_node->p_cuvw )
    {
        FREENULL( p_node->p_sample );
    }

    free( p_node );
    block_Release( p_block ); *pp_block = NULL;
    
    return NULL;
}

/*****************************************************************************
 * CloseDecoder: clean up the decoder
 *****************************************************************************/
static void CloseDecoder( vlc_object_t *p_this )
{
    decoder_t *p_dec = (decoder_t *)p_this;
    decoder_sys_t *p_sys = p_dec->p_sys;

    free( p_sys );
}



