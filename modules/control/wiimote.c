/*****************************************************************************
 * wiimote.c : wiimote module for vlc
 *****************************************************************************
 * Copyright (C) 2009 the VideoLAN team
 * $Id$
 *
 * Author: Leon Moctezuma <densedev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/

#include <fcntl.h>

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_interface.h>
#include <vlc_input.h>
#include <vlc_vout.h>
#include <vlc_aout.h>
#include <vlc_osd.h>
#include <vlc_playlist.h>
#include <cwiid.h>

#define BATTERY_STR_LEN	14
#define CHANNELS_NUMBER 4
#define VOLUME_TEXT_CHAN     p_global_intf->p_sys->p_channels[ 0 ]
#define VOLUME_WIDGET_CHAN   p_global_intf->p_sys->p_channels[ 1 ]


/*****************************************************************************
 * Module descriptor
 *****************************************************************************/
static int  Open    ( vlc_object_t * );
static void Close   ( vlc_object_t * );

vlc_module_begin ()
    set_shortname( N_("Wiimote") )
    set_category( CAT_INTERFACE )
    set_subcategory( SUBCAT_INTERFACE_CONTROL )
    set_description( N_("Wiimote control interface") )
    set_capability( "interface", 0 )
    set_callbacks( Open, Close )

    /*add_string( "lirc-file", NULL, NULL,
                LIRC_TEXT, LIRC_LONGTEXT, true )*/
vlc_module_end ()

/*****************************************************************************
 * intf_sys_t: description and status of FB interface
 *****************************************************************************/
struct intf_sys_t
{
    cwiid_wiimote_t* p_wiimote;	       /* wiimote handle */
    struct cwiid_state state;	       /* wiimote state */
    bdaddr_t bdaddr;	               /* bluetooth device address */
    int p_channels[ CHANNELS_NUMBER ]; /* contains registered
                                        * channel IDs */
    uint16_t status;
};

/*****************************************************************************
 * Local prototypes
 *****************************************************************************/
static void Run( intf_thread_t * );
void cwiid_callback(cwiid_wiimote_t *wiimote, int mesg_count,
                    union cwiid_mesg mesg[], struct timespec *timestamp);
void cwiid_btn(struct cwiid_btn_mesg *mesg);

static void DisplayVolume( intf_thread_t *p_intf, vout_thread_t *p_vout,
                           audio_volume_t i_vol );
static void ClearChannels( intf_thread_t *p_intf, vout_thread_t *p_vout );

/*****************************************************************************
 * Open: initialize interface
 *****************************************************************************/
static int Open( vlc_object_t *p_this )
{
    intf_thread_t *p_intf = (intf_thread_t *)p_this;
    intf_sys_t *p_sys;

    p_intf->p_sys = p_sys = malloc( sizeof( intf_sys_t ) );
    if( p_sys == NULL )
        return VLC_ENOMEM;

    p_sys->bdaddr = *BDADDR_ANY;

    p_intf->pf_run = Run;

    msg_Err(p_intf, "Wiimote\n");
    return VLC_SUCCESS;
}

/*****************************************************************************
 * Close: destroy interface
 *****************************************************************************/
static void Close( vlc_object_t *p_this )
{
    intf_thread_t *p_intf = (intf_thread_t *)p_this;
    intf_sys_t *p_sys = p_intf->p_sys;
    
    free( p_sys );
}

intf_thread_t *p_global_intf = NULL;

/*****************************************************************************
 * Run: main loop
 *****************************************************************************/
static void Run( intf_thread_t *p_intf )
{
    p_global_intf = p_intf;
    intf_sys_t *p_sys = p_intf->p_sys;
    struct cwiid_state state;
    p_sys->status = 0;

    msg_Err(p_intf, "Put Wiimote in discoverable mode (press 1+2) to connect it...\n");
    while(!(p_sys->p_wiimote = cwiid_open(&p_sys->bdaddr, 0)));

    if (cwiid_set_mesg_callback(p_sys->p_wiimote, cwiid_callback)) {
        msg_Err(p_intf, "Unable to set message callback");
        return;
    }

    if( cwiid_enable(p_sys->p_wiimote, CWIID_FLAG_MESG_IFC) ) {
        msg_Err(p_intf, "Error enabling messages\n");
        return;
    }

    p_sys->status++;
    cwiid_set_rpt_mode( p_sys->p_wiimote, CWIID_RPT_BTN );
    cwiid_set_led( p_sys->p_wiimote, p_sys->status );

    return;

}

void cwiid_btn(struct cwiid_btn_mesg *mesg)
{
    vout_thread_t *p_vout = NULL;
    aout_instance_t *p_aout = NULL;
    playlist_t *p_playlist = pl_Hold( p_global_intf );
    input_thread_t *p_input;
    intf_sys_t *p_sys = p_global_intf->p_sys;

    /* Update the input */
    p_input = playlist_CurrentInput( p_playlist );

    /* Update the vout */
    p_vout = p_input ? input_GetVout( p_input ) : NULL;

    /* Update the aout */
    p_aout = p_input ? input_GetAout( p_input ) : NULL;
    
    if ( mesg->buttons & CWIID_BTN_2 ){
        vlc_value_t val, list, list2;
        int i_count, i;
        
        msg_Dbg( p_global_intf, "2" );

        //in case there is no file open
        if(p_input != NULL){

        var_Get( p_input, "audio-es", &val );

        var_Change( p_input, "audio-es", VLC_VAR_GETCHOICES,
                        &list, &list2 );
      
        i_count = list.p_list->i_count;

        if( i_count > 1 )
        {

                for( i = 0; i < i_count; i++ )
                {
                        if( val.i_int == list.p_list->p_values[i].i_int )
                        {
                            break;
                        }
                }

                /* value of audio-es was not in choices list */
                if( i == i_count )
                {
                       msg_Warn( p_input,
                                  "invalid current audio track, selecting 0" );
                       i = 0;
                }
                else if( i == i_count - 1 )
                        i = 0;
                else
                        i++;
                var_Set( p_input, "audio-es", list.p_list->p_values[i] );
            }
        }
    }

    if ( mesg->buttons & CWIID_BTN_1 )
    {

        vlc_value_t val, list, list2;
        int i_count, i;

        msg_Dbg( p_global_intf, "1" );
        
        //in case there is no file open
        if(p_input != NULL){

            var_Get( p_input, "spu-es", &val );

            var_Change( p_input, "spu-es", VLC_VAR_GETCHOICES,
                    &list, &list2 );

            i_count = list.p_list->i_count;

            if( i_count > 1 )
            {
                       
               for( i = 0; i < i_count; i++ )
               {
                    if( val.i_int == list.p_list->p_values[i].i_int )
                    {
                       break;
                    }
                }
                /* value of spu-es was not in choices list */
                if( i == i_count )
                {
                    msg_Warn( p_input,
                             "invalid current subtitle track, selecting 0" );
                    i = 0;
                }
                else if( i == i_count - 1 )
                    i = 0;
                else
                    i++;

                var_Set( p_input, "spu-es", list.p_list->p_values[i] );

            }
        }
    }

    if ( mesg->buttons & CWIID_BTN_B ) {
        msg_Dbg( p_global_intf, "B" );
        if(p_sys->status >= 0x08) p_sys->status = 1;
        else p_sys->status=p_sys->status << 1;
        cwiid_set_led( p_sys->p_wiimote, p_sys->status );
    }
    
    if ( mesg->buttons & CWIID_BTN_A ) {
        if( p_input )
        {
               ClearChannels( p_global_intf, p_vout );

               int state = var_GetInteger( p_input, "state" );
               if( state != PAUSE_S )
               {
                   vout_OSDIcon( VLC_OBJECT( p_global_intf ), DEFAULT_CHAN,
                                 OSD_PAUSE_ICON );
                   state = PAUSE_S;
               }
               else
               {
                   vout_OSDIcon( VLC_OBJECT( p_global_intf ), DEFAULT_CHAN,
                                 OSD_PLAY_ICON );
                   state = PLAYING_S;
               }
               var_SetInteger( p_input, "state", state );
        }
        else
        {
               playlist_Play( p_playlist );
        }
        msg_Dbg( p_global_intf, "A" );
    }
    
    if ( mesg->buttons & CWIID_BTN_HOME ){
        msg_Dbg( p_global_intf, "HOME" );
    }
    
    if ( mesg->buttons & CWIID_BTN_LEFT ){ 
        if( p_input )
        { 
            msg_Dbg( p_global_intf, "LEFT" );
            vout_OSDMessage( VLC_OBJECT(p_input), DEFAULT_CHAN, _("Previous") );
            playlist_Previous( p_playlist );
        }
    }
    
    if ( mesg->buttons & CWIID_BTN_RIGHT ){
        if( p_input )
        { 
            msg_Dbg( p_global_intf, "RIGHT" );
            vout_OSDMessage( VLC_OBJECT(p_input), DEFAULT_CHAN, _("Next") );
            playlist_Next( p_playlist );
        }
    }

    if ( mesg->buttons & CWIID_BTN_UP ){
        msg_Dbg( p_global_intf, "UP" );
    }

    if ( mesg->buttons & CWIID_BTN_DOWN ){
        msg_Dbg( p_global_intf, "DOWN" );
    }
    
    if ( mesg->buttons & CWIID_BTN_PLUS ){
        audio_volume_t i_newvol;
        aout_VolumeUp( p_global_intf, 1, &i_newvol );
        DisplayVolume( p_global_intf, p_vout, i_newvol );
        msg_Dbg( p_global_intf, "PLUS" );
    }
    
    if ( mesg->buttons & CWIID_BTN_MINUS ){
        audio_volume_t i_newvol;
        aout_VolumeDown( p_global_intf, 1, &i_newvol );
        DisplayVolume( p_global_intf, p_vout, i_newvol );
        msg_Dbg( p_global_intf, "MINUS" );
    }

    if( p_aout )
        vlc_object_release( p_aout );
    if( p_vout )
        vlc_object_release( p_vout );
    if( p_input )
        vlc_object_release( p_input );
    if( p_input )
        vlc_object_release( p_playlist );

}

void cwiid_callback(cwiid_wiimote_t *wiimote, int mesg_count,
                    union cwiid_mesg mesg_array[], struct timespec *timestamp)
{
    	int i;
	char battery[BATTERY_STR_LEN];
	char *ext_str;
	static enum cwiid_ext_type ext_type = CWIID_EXT_NONE;

	for (i=0; i < mesg_count; i++) {
		switch (mesg_array[i].type) {
		case CWIID_MESG_STATUS:
			snprintf(battery, BATTERY_STR_LEN,"Battery:%d%%",
			         (int) (100.0 * mesg_array[i].status_mesg.battery /
			                CWIID_BATTERY_MAX));
			printf("%s\n", battery);
			/*switch (mesg_array[i].status_mesg.ext_type) {
			case CWIID_EXT_NONE:
				ext_str = "No extension";
				break;
			case CWIID_EXT_NUNCHUK:
				ext_str = "Nunchuk";
				if (ext_type != CWIID_EXT_NUNCHUK) {
					if (cwiid_get_acc_cal(wiimote, CWIID_EXT_NUNCHUK,
					                      &nc_cal)) {
						message(GTK_MESSAGE_ERROR,
						        "Unable to retrieve accelerometer calibration",
						        GTK_WINDOW(winMain));
					}
				}
				break;
			case CWIID_EXT_CLASSIC:
				ext_str = "Classic controller";
				break;
			case CWIID_EXT_UNKNOWN:
				ext_str = "Unknown extension";
				break;
			}*/
			break;
		case CWIID_MESG_BTN:
			cwiid_btn(&mesg_array[i].btn_mesg);
			break;
		case CWIID_MESG_ACC:
			//cwiid_acc(&mesg_array[i].acc_mesg);
			break;
		case CWIID_MESG_IR:
			//cwiid_ir(&mesg_array[i].ir_mesg);
			break;
		case CWIID_MESG_NUNCHUK:
			//cwiid_nunchuk(&mesg_array[i].nunchuk_mesg);
			break;
		case CWIID_MESG_CLASSIC:
			//cwiid_classic(&mesg_array[i].classic_mesg);
			break;
		case CWIID_MESG_ERROR:
			//menuDisconnect_activate();
			break;
		default:
			break;
		}
	}
}

static void DisplayVolume( intf_thread_t *p_intf, vout_thread_t *p_vout,
                           audio_volume_t i_vol )
{
    if( p_vout == NULL )
    {
        return;
    }
    ClearChannels( p_intf, p_vout );

    if( p_vout->b_fullscreen )
    {
        vout_OSDSlider( VLC_OBJECT( p_vout ), VOLUME_WIDGET_CHAN,
            i_vol*100/AOUT_VOLUME_MAX, OSD_VERT_SLIDER );
    }
    else
    {
        vout_OSDMessage( p_vout, VOLUME_TEXT_CHAN, _( "Volume %d%%" ),
                         i_vol*400/AOUT_VOLUME_MAX );
    }
}

static void ClearChannels( intf_thread_t *p_intf, vout_thread_t *p_vout )
{
    int i;

    if( p_vout )
    {
        spu_Control( p_vout->p_spu, SPU_CHANNEL_CLEAR, DEFAULT_CHAN );
        for( i = 0; i < CHANNELS_NUMBER; i++ )
        {
            spu_Control( p_vout->p_spu, SPU_CHANNEL_CLEAR,
                         p_intf->p_sys->p_channels[ i ] );
        }
    }
}
